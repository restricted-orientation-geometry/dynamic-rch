Dynamic RCH
===========

A set of dynamic geometry applications that let the user to visualize and modify
three variations of the Rectilinear Convex Hull of a planar point set [1]. The
applications were developed using Java, JavaFX, and Maven:

* DynamicRCH - Allows adding, removing, and moving points, while automatically
    computing the Rectilinear Convex Hull of the point set. It also presents a
    framework to ease the creation of interactive applications to visualize
    variations of the standard Rectilinear Convex Hull of a point set.
* DynamicThetaRCH - An extension of DynamicRCH that allows the coordinate axis
    to be rotated.
* DynamicBetaHull - An extension of DynamicRCH that allows the angle between
    the coordinate axis to be changed [2].

Requirements
------------

All dependencies are handled by Maven, and referenced from Maven Central
Repository [3]. All three applications have at least one of the following
dependencies:

* JavaGeom 0.11.1+
* SLF4j + Java Logging support 1.7.12+
* TestNG 6.9.8+

References
----------

* [1] https://en.wikipedia.org/wiki/Orthogonal_convex_hull
* [2] https://doi.org/10.1016/j.comgeo.2017.06.003
* [3] https://search.maven.org/
