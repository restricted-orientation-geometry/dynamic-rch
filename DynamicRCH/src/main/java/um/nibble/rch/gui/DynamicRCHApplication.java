/*
 * This file is part of Dynamic RCH.
 *
 * Copyright 2015 Carlos Alegría Galicia
 *
 * Dynamic RCH is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or any later version.
 *
 * Dynamic RCH is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Dynamic RCH. If not, see <http://www.gnu.org/licenses/>.
 */
package um.nibble.rch.gui;

import java.util.Arrays;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

/**
 * @author Carlos Alegría Galicia
 *
 */
public class DynamicRCHApplication extends Application {

	private static final String CSS_FILE = "application.css";
	private static final KeyCombination FULL_SCREEN_ON = KeyCombination.valueOf("F5");

	/*
	 * (non-Javadoc)
	 * 
	 * @see javafx.application.Application#start(javafx.stage.Stage)
	 */
	@Override
	public final void start(Stage stage) throws Exception {
		stage.setScene(getScene());
		stage.setTitle(getTitle());
		stage.setFullScreen(true);
		stage.fullScreenProperty().addListener((observable, oldValue, newValue) -> {
			if (!newValue) {
				stage.setMaximized(true);
			}
		});
		stage.addEventFilter(KeyEvent.KEY_PRESSED, (final KeyEvent event) -> {
			if (FULL_SCREEN_ON.match(event) && !stage.isFullScreen()) {
				stage.setFullScreen(true);
				event.consume();
			}
		});

		configureStage(stage);
		stage.show();
	}

	/**
	 * @return
	 */
	private Scene getScene() {
		// using Group as parent so DBHNode can receive key events
		//
		Scene scene = new Scene(new BorderPane(getNode()));

		// adding basic CSS
		//
		scene.getStylesheets().add(ClassLoader.getSystemResource(CSS_FILE).toExternalForm());

		// adding additional CSS files
		//
		Arrays.stream(getCSS())
				.forEach(css -> scene.getStylesheets().add(ClassLoader.getSystemResource(css).toExternalForm()));

		return scene;
	}

	/**
	 * 
	 */
	protected void configureStage(Stage stage) {
	}

	/**
	 * @return
	 */
	protected String getTitle() {
		return "Dynamic RCH";
	}

	/**
	 * @return
	 */
	protected DynamicRCHNode getNode() {
		return new DynamicRCHNode();
	}

	/**
	 * @return
	 */
	protected String[] getCSS() {
		return new String[0];
	}
}
