/*
 * This file is part of Dynamic RCH.
 *
 * Copyright 2015 Carlos Alegría Galicia
 *
 * Dynamic RCH is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or any later version.
 *
 * Dynamic RCH is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Dynamic RCH. If not, see <http://www.gnu.org/licenses/>.
 */
package um.nibble.rch.gui;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.SetChangeListener;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Region;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Polyline;
import um.nibble.rch.model.DynamicRCHModel;
import um.nibble.rch.model.Point;

/**
 * @author Carlos Alegría Galicia
 *
 */
public class DynamicRCHNode extends Region {
	
	private static final String MAXIMAL_CSS_CLASS = "point_maximal";
	private static final String NORMAL_CSS_CLASS = "point_normal";
	private static final String POINT_CSS_CLASS = "point";
	private static final String COMPONENT_CSS_CLASS = "component";
	private static final String OVERLAP_CSS_CLASS = "overlap";
	private static final String FIRST_STAIRCASE_CSS_CLASS = "first_staircase";
	private static final String SECOND_STAIRCASE_CSS_CLASS = "second_staircase";
	private static final String THIRD_STAIRCASE_CSS_CLASS = "third_staircase";
	private static final String FOURTH_STAIRCASE_CSS_CLASS = "fourth_staircase";
	
	private static final int RADIUS = 5;
	
	protected static final KeyCombination STAIRCASE_TOGGLE = KeyCombination.valueOf("ctrl+s");
	protected static final KeyCombination RCH_TOGGLE = KeyCombination.valueOf("ctrl+r");
	
	
	//	
	// components and overlaps variables
	//
	
	// shape groups
	//
	private final Group components;
	private final Group overlaps;
	
	// change listeners
	//
	private final SetChangeListener<double []> componentsListener;
	private final SetChangeListener<double []> overlapsListener;
	
	// visibility flag
	//
	private boolean rchVisible;
	
	
	//
	// staircases variables
	//
	
	// shape groups
	//
	private final Group firstStaircase;
	private final Group secondStaircase;
	private final Group thirdStaircase;
	private final Group fourthStaircase;
	
	// change listeners
	//
	private final ListChangeListener<Double> firstStaircaseListener;
	private final ListChangeListener<Double> secondStaircaseListener;
	private final ListChangeListener<Double> thirdStaircaseListener;
	private final ListChangeListener<Double> fourthStaircaseListener;
	
	// visibility flag
	//
	private boolean staircasesVisible;
	
	private final DynamicRCHModel model;
	private final Logger logger = LoggerFactory.getLogger(DynamicRCHNode.class);
	
	/**
	 * Viewer -> Model communication is done through point addition
	 * Model  -> Viewer communication is done through orientation and staircases
	 * 			 properties binding
	 */
	public DynamicRCHNode() {
		this(new DynamicRCHModel());
	}

	/**
	 * @param model
	 */
	public DynamicRCHNode(DynamicRCHModel model) {
		super();
		this.model = model;
		
		
		//
		// components and overlaps
		//
		
		this.rchVisible = true;
		
		//
		// shapes
		//
		
		this.components = new Group();
		this.overlaps = new Group();
		
		getChildren().add(this.components);
		getChildren().add(this.overlaps);
		
		//
		// listeners
		//
		
		this.componentsListener = createSetChangeListener(
				"Components", COMPONENT_CSS_CLASS, this.components);
		this.overlapsListener = createSetChangeListener(
				"Overlaps", OVERLAP_CSS_CLASS, this.overlaps);
		
		this.model.components().addListener(this.componentsListener);
		this.model.overlaps().addListener(this.overlapsListener);

		
		//
		// staircases
		//
		
		this.staircasesVisible = true;
		
		//
		// shapes
		//
		
		this.firstStaircase = new Group();
		this.secondStaircase = new Group();
		this.thirdStaircase = new Group();
		this.fourthStaircase = new Group();
		
		getChildren().add(this.firstStaircase);
		getChildren().add(this.secondStaircase);
		getChildren().add(this.thirdStaircase);
		getChildren().add(this.fourthStaircase);
		
		//
		// change listeners
		//
		
		this.firstStaircaseListener = createStaircaseChangeListener(
				"First staircase", FIRST_STAIRCASE_CSS_CLASS, this.firstStaircase);
		this.secondStaircaseListener = createStaircaseChangeListener(
				"Second staircase", SECOND_STAIRCASE_CSS_CLASS, this.secondStaircase);
		this.thirdStaircaseListener = createStaircaseChangeListener(
				"Third staircase", THIRD_STAIRCASE_CSS_CLASS, this.thirdStaircase); 
		this.fourthStaircaseListener = createStaircaseChangeListener(
				"Fourth staircase", FOURTH_STAIRCASE_CSS_CLASS, this.fourthStaircase); 
		
		this.model.firstStaircase().addListener(this.firstStaircaseListener);
		this.model.secondStaircase().addListener(this.secondStaircaseListener);
		this.model.thirdStaircase().addListener(this.thirdStaircaseListener);
		this.model.fourthStaircase().addListener(this.fourthStaircaseListener);
		
		
		//
		// key events
		//
		
		setOnKeyPressed((final KeyEvent event) -> {
			
			// toggle staircases visibility
			//
			if (STAIRCASE_TOGGLE.match(event)) {
				if (this.staircasesVisible) {
					this.model.firstStaircase().removeListener(this.firstStaircaseListener);
					this.model.secondStaircase().removeListener(this.secondStaircaseListener);
					this.model.thirdStaircase().removeListener(this.thirdStaircaseListener);
					this.model.fourthStaircase().removeListener(this.fourthStaircaseListener);
					
					this.firstStaircase.getChildren().clear();
					this.secondStaircase.getChildren().clear();
					this.thirdStaircase.getChildren().clear();
					this.fourthStaircase.getChildren().clear();
					
					this.staircasesVisible = false;
				} else {
					this.model.firstStaircase().addListener(this.firstStaircaseListener);
					this.model.secondStaircase().addListener(this.secondStaircaseListener);
					this.model.thirdStaircase().addListener(this.thirdStaircaseListener);
					this.model.fourthStaircase().addListener(this.fourthStaircaseListener);
					this.model.update();
					
					this.staircasesVisible = true;
				}
				
				event.consume();
			} else if (RCH_TOGGLE.match(event)) {
				if (this.rchVisible) {
					this.model.components().removeListener(this.componentsListener);
					this.model.overlaps().removeListener(this.overlapsListener);
					
					this.components.getChildren().clear();
					this.overlaps.getChildren().clear();
					
					this.rchVisible = false;
				} else {
					this.model.components().addListener(this.componentsListener);
					this.model.overlaps().addListener(this.overlapsListener);
					this.model.update();
					
					this.rchVisible = true;
				}
				
				event.consume();
			}
		});
		
		
		//
		// points
		//
		// adding points with mouse (removal is done as a listener on every
		// circle)
		//
		
		setOnMousePressed((final MouseEvent event) -> {
			if (event.getButton() == MouseButton.PRIMARY) {
				getChildren().add(createPoint(event.getX(), event.getY()));
			}
		});
		
		
		//
		// enabling key events
		//
		
		setFocusTraversable(true);
	}
	
	/**
	 * @param label
	 * @param cssClass
	 * @param group
	 * @return
	 */
	private ListChangeListener<Double> createStaircaseChangeListener(
			String label, String cssClass, Group group) {
		
		return (ListChangeListener<Double>) change -> {
			
			ObservableList<Node> children = group.getChildren();
			
			while (change.next()) {
				
				logger.debug("{} list modified, {}", label, change);

				if (change.wasRemoved() && !children.isEmpty()) {
					children.clear();
				}
				
				if (change.wasAdded()) {
					
					// creating polygon
					//
					Polyline poly = new Polyline();
					poly.getStyleClass().add(cssClass);
					
					ObservableList<Double> pPoints = poly.getPoints();
					change.getAddedSubList().forEach(d -> pPoints.add(StrictMath.abs(d)));
					
					// adding polygon to staircases
					//
					children.add(poly);
				}
			}
		};
	}
	
	/**
	 * @param label
	 * @param cssClass
	 * @param group
	 * @return
	 */
	private SetChangeListener<double []> createSetChangeListener(
			String label, String cssClass, Group group) {
		
		return (SetChangeListener<double[]>) change -> {
			ObservableList<Node> children = group.getChildren();
			
			logger.debug("{} set modified, {}", label, change);

			if (change.wasRemoved() && !children.isEmpty()) {
				children.clear();
			}
			
			if (change.wasAdded()) {
				
				// creating polygon
				//
				Polygon poly = new Polygon();
				poly.getStyleClass().add(cssClass);

				ObservableList<Double> pPoints = poly.getPoints();
				Arrays.stream(change.getElementAdded()).forEach(d -> pPoints.add(StrictMath.abs(d)));

				// adding polygon to staircases
				//
				children.add(poly);
			}
		};
	}
	
	/**
	 * @param x
	 * @param y
	 * @return
	 */
	private Node createPoint(double x, double y) {

		final DragContext context = new DragContext();
		final Circle circle = new Circle(x, y, RADIUS);
		final Node wrap = new Group(circle);

		circle.getStyleClass().addAll(POINT_CSS_CLASS, NORMAL_CSS_CLASS);

		//
		// dragging and deletion
		//

		wrap.addEventFilter(
				MouseEvent.MOUSE_PRESSED,
				(final MouseEvent event) -> {
					
					switch (event.getButton()) {
					case PRIMARY: // start dragging circle
						
						context.anchorX = event.getX();
						context.anchorY = event.getY();
						context.translateX = circle.getTranslateX();
						context.translateY = circle.getTranslateY();
						break;
						
					case SECONDARY: // destroying circle and point
						
						getChildren().remove(wrap);
						model.removePoint((Point) circle.getUserData());
						break;
						
					default:
						break;
					}
					event.consume();
				});
		wrap.addEventFilter(
				MouseEvent.MOUSE_DRAGGED,
				(final MouseEvent event) -> {
					
					final double translateX = context.translateX + event.getX() - context.anchorX;
					final double translateY = context.translateY + event.getY() - context.anchorY;
					
					// translate to new position if it is greater than circle
					// radius and does not exceeds borders of the pane
					//
					circle.setTranslateX(translateX);
					circle.setTranslateY(translateY);
				});
		
		//
		// setup mirroring point in model
		// 
		
		Point point = new Point(circle.getCenterX(), -circle.getCenterY());
		
		// point maximality defines circle CSS style
		//
		point.maximalProperty().addListener((ObservableValue<? extends Boolean> ov,
				Boolean oldValue, Boolean newValue) -> {
			if (oldValue == newValue) {
				return;
			}
			if (newValue) {
				circle.getStyleClass().remove(NORMAL_CSS_CLASS);
				circle.getStyleClass().add(MAXIMAL_CSS_CLASS);
			} else {
				circle.getStyleClass().remove(MAXIMAL_CSS_CLASS);
				circle.getStyleClass().add(NORMAL_CSS_CLASS);
			}
		});
		
		// circle translation changes point x/y values
		//
		point.xProperty().bind(circle.centerXProperty().add(circle.translateXProperty()));
		point.yProperty().bind(circle.centerYProperty().add(circle.translateYProperty()).negate());
		
		circle.setUserData(point);
		this.model.addPoint(point);
		return wrap;
	}
	
	/**
	 * @return
	 */
	public DynamicRCHModel getModel() {
		return this.model;
	}
	
	/**
	 * @author Carlos Alegría
	 *
	 */
	private static final class DragContext {
		public double anchorX;
		public double anchorY;
		public double translateX;
		public double translateY;
		
		@Override
		public String toString() {
			return "(" + anchorX + ", " + anchorY + ", " + translateX + ", "
					+ translateY + ")";
		}
	}
}
