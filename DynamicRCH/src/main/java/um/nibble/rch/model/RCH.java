/*
 * This file is part of Dynamic RCH.
 *
 * Copyright 2015 Carlos Alegría Galicia
 *
 * Dynamic RCH is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or any later version.
 *
 * Dynamic RCH is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Dynamic RCH. If not, see <http://www.gnu.org/licenses/>.
 */
package um.nibble.rch.model;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import math.geom2d.Point2D;
import math.geom2d.line.LineSegment2D;
import math.utils.EqualUtils;

/**
 * @author Carlos Alegría Galicia
 */
public final class RCH {

	private static final Logger logger = LoggerFactory.getLogger(RCH.class);
	
	// Provides a total increasing lexicographical order. Compares the
	// projection on X axis along variable axis. Ties are solved using Y
	// coordinate
	//
	private static final Comparator<Point> COMP = (p1, p2) -> {
		if (p1.getX() > p2.getX()) {
			return 1;
		}
		if (p1.getX() < p2.getX()) {
			return -1;
		}
		if (p1.getY() > p2.getY()) {
			return 1;
		}
		if (p1.getY() < p2.getY()) {
			return -1;
		}
		return 0;
	};
	
	private LinkedList<Double> inFirstStaircase = new LinkedList<>();
	private LinkedList<Double> inSecondStaircase = new LinkedList<>();
	private LinkedList<Double> inThirdStaircase = new LinkedList<>();
	private LinkedList<Double> inFourthStaircase = new LinkedList<>();
	
	public Set<double[]> overlaps = new HashSet<>();
	public Set<double[]> components = new HashSet<>();
	
	public List<Double> firstStaircase = inFirstStaircase;
	public List<Double> secondStaircase = inSecondStaircase;
	public List<Double> thirdStaircase = inThirdStaircase;
	public List<Double> fourthStaircase = inFourthStaircase;
	
	
	/**
	 * @param points
	 * @param rch
	 */
	public static void computeRCH(LinkedList<Point> points, RCH rch) {
		
		//
		// clear rch representation
		//
		
		rch.components.clear();
		rch.overlaps.clear();
		rch.firstStaircase.clear();
		rch.secondStaircase.clear();
		rch.thirdStaircase.clear();
		rch.fourthStaircase.clear();
		
		
		//
		// trivial hulls
		//
		
		switch (points.size()) {
		case 1:
			points.get(0).setMaximal(true);
		case 0:
			return;
		}
		
		
		//
		// computing staircases
		//

		points.forEach(p -> p.setMaximal(false));
		Collections.sort(points, COMP);
		
		// first and fourth
		//
		computeStaircase(rch.inFirstStaircase, points.descendingIterator(), true);
		computeStaircase(rch.inFourthStaircase,points.descendingIterator(), false);

		// second and third
		//
		computeStaircase(rch.inSecondStaircase, points.iterator(), true);
		computeStaircase(rch.inThirdStaircase, points.iterator(), false);
		
		// So far, vertices in staircases 2 and 4 are ordered increasingly along
		// the x-axis, and staircases 1 and 3 decreasingly along the same axis.
		//
		// Staircases 2 and 4 have to be reversed in order to present the four
		// of them in the ccw order str1 -> str2 -> str3 -> str4
		//
		reverse(rch.secondStaircase);
		reverse(rch.fourthStaircase);
		
		logger.debug("first staircase computed = {}", Arrays.toString(rch.inFirstStaircase.stream().toArray()));
		logger.debug("second staircase computed = {}", Arrays.toString(rch.inSecondStaircase.stream().toArray()));
		logger.debug("third staircase computed = {}", Arrays.toString(rch.inThirdStaircase.stream().toArray()));
		logger.debug("fourth staircase computed = {}", Arrays.toString(rch.inFourthStaircase.stream().toArray()));
		
		
		//
		// computing components and overlaps
		//
		
		List<Intersection> intersections;

		intersections = intersect(rch.inFirstStaircase, rch.inThirdStaircase, true);
		if (!intersections.isEmpty()) {

			//
			// first and third staircases intersect
			//
			
			logger.debug("first-third intersections = {}", Arrays.toString(intersections.stream().toArray()));
			
			construct(
					rch.inFirstStaircase,
					rch.inThirdStaircase,
					rch.inFourthStaircase,
					rch.inSecondStaircase,
					intersections,
					true,
					rch.components,
					rch.overlaps);
			return;
		}
		
		intersections = intersect(rch.inSecondStaircase, rch.inFourthStaircase, false);
		if (!intersections.isEmpty()) {

			//
			// second and fourth staircases intersect
			//
				
			logger.debug("second-fourth intersections = {}", Arrays.toString(intersections.stream().toArray()));
				
			construct(
					rch.inSecondStaircase,
					rch.inFourthStaircase,
					rch.inFirstStaircase,
					rch.inThirdStaircase,
					intersections,
					false,
					rch.components,
					rch.overlaps);
			return;
		}
		
		//
		// no intersections, there is a single component
		//
				
		logger.debug("no intersections exist");
				
		LinkedList<Double> component = new LinkedList<>(rch.firstStaircase);
		component.addAll(rch.secondStaircase);
		component.addAll(rch.thirdStaircase);
		component.addAll(rch.fourthStaircase);

		rch.components.add(
				component.stream().mapToDouble(Double::doubleValue).toArray());
	}

	/**
	 * <p>
	 * Computes the vertices that specify a staircase. The coordinates of the
	 * vertices are stored in the provided list in (x1,y1,x2,y2, ...) fashion.
	 * The list is assumed to be empty.
	 * </p>
	 * <p>
	 * After the computation, all maximal points in the point set given by the
	 * specified iterator supplier are marked using
	 * {@link Point#setMaximal(boolean)}. The point set is assumed to have at
	 * least two points.
	 * </p>
	 * 
	 * @param staircase
	 *            The list where staircase vertices are going to be stored.
	 * @param itSupplier
	 *            A supplier of an iterator to the point set.
	 * @param positive
	 *            True if Y axis is to be considered with positive (first and
	 *            second quadrants), or false otherwise (third and fourth
	 *            quadrants).
	 */
	private static void computeStaircase(LinkedList<Double> staircase,
			Iterator<Point> points, boolean positive) {
		
		//
		// selecting maximal elements
		//
			
		// first element is maximal
		//
		Point point = points.next();
		point.setMaximal(true);
		
		staircase.add(point.getX());
		staircase.add(point.getY());
		
		// traversing the rest of the set
		//
		while (points.hasNext()) {
			point = points.next();
			if (positive
					? point.getY() > staircase.peekLast()
					: point.getY() < staircase.peekLast()) {
				point.setMaximal(true);
				staircase.add(point.getX());
				staircase.add(point.getY());
			}
		}
	
		// even though the point set has at least 2 points, for the current
		// settings, the number of maximal elements could be less than zero. If
		// this is the case, return with an empty staircase
		//
		if (staircase.size() < 4) {
			staircase.clear();
			return;
		}
		
		
		//
		// creating staircase
		//
		
		ListIterator<Double> it = staircase.listIterator();
		double currentY = Double.NaN;
		double nextX = it.next();
		double nextY = it.next();
		
		do {
			// setting up step extreme points
			//
			currentY = nextY;
			nextX = it.next();
			nextY = it.next();
			
			// rolling back cursor 
			//
			it.previous();
			it.previous();
			
			// adding corner
			//
			it.add(nextX);
			it.add(currentY);

			// rolling forward cursor
			//
			it.next();
			it.next();
			
		} while (it.hasNext());
	}
	
	/**
	 * @param right
	 * @param opposite
	 * @return
	 */
	private static List<Intersection> intersect(LinkedList<Double> right,
			LinkedList<Double> opposite, boolean first) {
		
		// if either staircase is empty, return an empty set of intersections
		//
		if (right.isEmpty() || opposite.isEmpty()) {
			return Collections.emptyList();
		}
		
		
		//
		// local variables
		//
		
		final LinkedList<Intersection> result = new LinkedList<>();
		final Iterator<Double> rIt = right.iterator();
		final Iterator<Double> opIt = opposite.descendingIterator();
		
		// initial end point of right staircase current segment
		//
		double rStartX = Double.NaN;
		double rStartY = Double.NaN;
		
		// final end point of right staircase current segment
		//
		double rEndX = rIt.next();
		double rEndY = rIt.next();
		
		// initial end point of opposite staircase current segment
		//
		double opStartX = Double.NaN;
		double opStartY = Double.NaN;
		
		// final end point of opposite staircase current segment (opposite
		// staircase is traversed using descending iterator)
		//
		double opEndY = opIt.next();
		double opEndX = opIt.next();
		
		// current state of intersections computation
		//
		IntersectionState intState = IntersectionState.TEST_RIGHT;
		boolean withinIntersection = false;

		
		//
		// main cycle
		//
		
		MAIN_CYCLE:
		while (true) {
			switch (intState) {
			case TEST_RIGHT:
				
				//
				// test if the current (vertical) opposite segment is a
				// candidate for intersection with the current (horizontal)
				// right segment
				//
				
				if (first ? rEndY > opEndY : rEndX < opEndX) {
					
					//
					// skip opposite if current segment is not a candidate
					//
					
					if (!opIt.hasNext()) break MAIN_CYCLE;
					opStartX = opEndX;
					opStartY = opEndY;
					
					opEndY = opIt.next();
					opEndX = opIt.next();
					
				} else {
					
					// right segment complies, need to test opposite segment
					//
					intState = IntersectionState.TEST_OPPOSITE;
				}
				
				break;
			case TEST_OPPOSITE:
				
				//
				// test if the current (horizontal) right segment is a candidate
				// for intersection with the current (vertical) opposite segment
				//
				
				if (first ? opEndX < rEndX : opEndY < rEndY) {
					
					//
					// skip right if current segment is not a candidate
					//
					
					if (!rIt.hasNext()) break MAIN_CYCLE;
					rStartX = rEndX;
					rStartY = rEndY;
					
					rEndX = rIt.next();
					rEndY = rIt.next();
					
				} else if (first ? rEndY > opEndY : rEndX < opEndX) {
					
					// opposite segment does not comply, start the cycle again
					//
					intState = IntersectionState.TEST_RIGHT;
					
				} else {
					
					// both segments comply, intersect
					//
					intState = IntersectionState.INTERSECT;
				}
				
				break;
			case INTERSECT:
				
				//
				// compute intersections between segments, and move cursors
				// accordingly to start cycle again
				//
				
				if (EqualUtils.areEqual(rEndX, opEndX)
						&& EqualUtils.areEqual(rEndY, opEndY)) {
					
					//
					// segments are reaching a common maximal
					//
					
					logger.debug(
							"segments reaching a common maximal: {},{},{},{}",
							rEndX, rEndY, opEndX, opEndY);
					
					result.add(new Intersection(rEndX, rEndY, true));
					
					//
					// skipping two segments in right staircase
					//
					
					if (!rIt.hasNext()) break MAIN_CYCLE;
					rStartX = rIt.next();
					rStartY = rIt.next();
					
					if (!rIt.hasNext()) break MAIN_CYCLE;
					rEndX = rIt.next();
					rEndY = rIt.next();
					
					//
					// skipping two segments in opposite staircases
					//
					
					if (!opIt.hasNext()) break MAIN_CYCLE;
					opStartY = opIt.next();
					opStartX = opIt.next();
					
					
					if (!opIt.hasNext()) break MAIN_CYCLE;
					opEndY = opIt.next();
					opEndX = opIt.next();
					
					// indicate that we are within an intersection chain
					//
					withinIntersection = true;
					
				} else {
					
					//
					// proper intersection
					//
					
					LineSegment2D s1 = new LineSegment2D(rStartX, rStartY, rEndX, rEndY);
					LineSegment2D s2 = new LineSegment2D(opStartX, opStartY, opEndX, opEndY);
					
					if (LineSegment2D.intersects(s1, s2)) {
						
						logger.debug(
								"segment intersection: {},{},{},{} - {},{},{},{}",
								rStartX, rStartY, rEndX, rEndY,
								opStartX, opStartY, opEndX, opEndY);
						
						// LineSegment2D.intersection does not handle V
						// intersections correctly
						//
						Point2D p = s1.intersection(s2);
						result.add(new Intersection(p.x(), p.y(), false));
					}
										
					//
					// skipping a segment in right staircase
					//
					
					if (!rIt.hasNext()) break MAIN_CYCLE;
					rStartX = rEndX;
					rStartY = rEndY;
					
					rEndX = rIt.next();
					rEndY = rIt.next();
					
					//
					// skipping a segment in opposite staircase
					//
					
					if (!opIt.hasNext()) break MAIN_CYCLE;
					opStartX = opEndX;
					opStartY = opEndY;
					
					opEndY = opIt.next();
					opEndX = opIt.next();
					
					if (withinIntersection) {
						
						//
						// a proper intersection ends an intersection chain
						//
						
						intState = IntersectionState.TEST_RIGHT;
						withinIntersection = false;
					} else {
						withinIntersection = true;
					}
				}

				break;
			}
		}
		
		return result;
	}
	
	/**
	 * @author Carlos Alegría Galicia
	 */
	private static enum IntersectionState {
		TEST_RIGHT, TEST_OPPOSITE, INTERSECT;
	}
	
	/**
	 * @param right
	 * @param opposite
	 * @param init
	 * @param fin
	 * @param intersections
	 * @param first
	 * @param components
	 * @param overlaps
	 */
	private static void construct(LinkedList<Double> right,
			LinkedList<Double> opposite, LinkedList<Double> init,
			LinkedList<Double> fin, List<Intersection> intersections,
			boolean first, Set<double[]> components, Set<double[]> overlaps) {

		//
		// local variables
		//
		
		boolean overlap;
		Intersection inPoint;
		LinkedList<Double> component;
		Iterator<Intersection> intIt;
		
		Iterator<Double> rightIt;
		Iterator<Double> oppositeIt;


		//
		// initial conditions
		//
		
		intIt = intersections.iterator();
		component = new LinkedList<>();
		
		rightIt = right.iterator();
		oppositeIt = opposite.descendingIterator();
		
		if (init.isEmpty()) {
			
			//
			// no initial staircase, first intersection is a maximal element
			//
			
			overlap = true;
			
			Intersection in = intIt.next();
			component.add(in.x);
			component.add(in.y);

		} else {
			
			//
			// there are elements in initial staircase, adding the points to the
			// first component
			//
			
			overlap = false;
			component.addAll(init);
		}
		
		
		//
		// main cycle
		//
		
		while (intIt.hasNext()) {

			inPoint = intIt.next();
			
			
			//
			// adding right staircase points
			//
			
			{
				double lastPointX = component.get(component.size() - 2);
				double lastPointY = component.getLast();
				
				while (true) {
					
					// next point in right staircase
					//
					double pointX = rightIt.next();
					double pointY = rightIt.next();
					
					// avoiding repeated points
					//
					if (!EqualUtils.areEqual(pointX, lastPointX)
							|| !EqualUtils.areEqual(pointY, lastPointY)) {
						
						// adding points to component, and updating last point
						// added
						//
						component.addLast(lastPointX = pointX);
						component.addLast(lastPointY = pointY);
					}
					
					// check limits
					//
					if ((first ? pointY >= inPoint.y : pointY <= inPoint.y) || pointX <= inPoint.x) {
						break;
					}
				}	
			}

			
			//
			// adding opposite staircase points
			//
			
			{
				double firstPointY = component.get(0);
				double firstPointX = component.get(1);
				
				while (true) {
					
					// previous point in opposite staircase
					//
					double pointY = oppositeIt.next();
					double pointX = oppositeIt.next();
					
					// avoiding repeated points
					//
					if (!EqualUtils.areEqual(pointX, firstPointX)
							|| !EqualUtils.areEqual(pointY, firstPointY)) {
						
						// adding points to component, and updating last point
						// added
						//
						component.addFirst(firstPointY = pointY);
						component.addFirst(firstPointX = pointX);
					}
					
					// check limits
					//
					if ((first ? pointY >= inPoint.y : pointY <= inPoint.y) || pointX <= inPoint.x) {
						break;
					}
				}
			}
			

			// adding final intersection point
			//
			component.add(inPoint.x);
			component.add(inPoint.y);

			
			//
			// updating overlap/components set
			//

			double arrComp[] = component.stream().mapToDouble(Double::doubleValue).toArray();
			if (overlap) {
				overlaps.add(arrComp);
			} else {
				components.add(arrComp);
			}

			
			//
			// updating state
			//
			
			if (!inPoint.maximal) {
				overlap = !overlap;
			}
			
			
			//
			// initializing next component
			//
			
			component.clear();
			component.add(inPoint.x);
			component.add(inPoint.y);
		};

		//
		// final component
		//
		
		if (!fin.isEmpty()) {
		
			//
			// adding right staircase points
			//
			
			while (rightIt.hasNext()) {
				double rPointX = rightIt.next();
				double rPointY = rightIt.next();
				
				// avoiding repeated points
				//
				if (rightIt.hasNext()) {
					component.addLast(rPointX);
					component.addLast(rPointY);
				}
			}

			//
			// adding opposite staircase points
			//
			
			while (oppositeIt.hasNext()) {
				
				double oppPointY = oppositeIt.next();
				double oppPointX = oppositeIt.next();
				
				// avoiding repeated points
				//
				if (oppositeIt.hasNext()) {
					component.addFirst(oppPointY);
					component.addFirst(oppPointX);
				}
			}
			
			component.addAll(fin);
			
			
			//
			// updating overlap/components set
			//
			
			double arrComp[] = component.stream().mapToDouble(Double::doubleValue).toArray();
			components.add(arrComp);
		}
	}
	
	@SuppressWarnings({"rawtypes", "unchecked"})
	private static void reverse(List<?> list) {
		int size = list.size();
		ListIterator fwd = list.listIterator();
        ListIterator rev = list.listIterator(size);
        for (int i = 0, mid = list.size()>>1; i < mid; i+=2) {
            Object tmpX = fwd.next();
            Object tmpY = fwd.next();
            
            fwd.set(rev.previous());
            rev.set(tmpY);
            
            // roll back to tmpX
            //
            fwd.previous();fwd.previous();
            
            fwd.set(rev.previous());
            rev.set(tmpX);
            
            // move cursor forward to tmpY 
            //
            fwd.next();fwd.next();
        }
	}
	
	/**
	 * @author Carlos Alegría Galicia
	 */
	private static class Intersection {
		
		double x;
		double y;
		boolean maximal;
		
		public Intersection(double x, double y, boolean maximal) {
			this.x = x;
			this.y = y;
			this.maximal = maximal;
		}

		@Override
		public String toString() {
			return "(" + this.x + "," + this.y + "," + this.maximal + ")";
		}
	}
}
