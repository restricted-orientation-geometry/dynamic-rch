/*
 * This file is part of Dynamic RCH.
 *
 * Copyright 2015 Carlos Alegría Galicia
 *
 * Dynamic RCH is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or any later version.
 *
 * Dynamic RCH is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Dynamic RCH. If not, see <http://www.gnu.org/licenses/>.
 */
package um.nibble.rch.model;

import java.util.LinkedList;

import javafx.beans.property.ListProperty;
import javafx.beans.property.ReadOnlyListProperty;
import javafx.beans.property.ReadOnlySetProperty;
import javafx.beans.property.SetProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleSetProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener.Change;

/**
 * @author Carlos Alegría
 */
public class DynamicRCHModel {
	
	// set of points
	//
	private final ListProperty<Point> points;
	private final LinkedList<Point> pointsList;
	
	// staircases
	//
	private final ListProperty<Double> firstStaircase;
	private final ListProperty<Double> secondStaircase;
	private final ListProperty<Double> thirdStaircase;
	private final ListProperty<Double> fourthStaircase;
	
	// list of overlaps
	//
	private final SetProperty<double[]> overlaps;
	
	// list of components
	//
	private final SetProperty<double[]> components;
	
	// rch internal representation
	//
	private final RCH rch;
		
	// used to trigger staircase computation when the points coordinates change
	//
	private final ChangeListener<? super Number> changeListener;
	

	/**
	 *
	 */
	public DynamicRCHModel() {

		// setting up internal representation
		//
		this.rch = new RCH();
		this.pointsList = new LinkedList<>();
		
		// setting up properties
		//
		this.points = new SimpleListProperty<>(FXCollections.observableList(this.pointsList));
		this.overlaps = new SimpleSetProperty<>(FXCollections.observableSet());
		this.components = new SimpleSetProperty<>(FXCollections.observableSet());
		this.firstStaircase = new SimpleListProperty<>(FXCollections.observableArrayList());
		this.secondStaircase = new SimpleListProperty<>(FXCollections.observableArrayList());
		this.thirdStaircase = new SimpleListProperty<>(FXCollections.observableArrayList());
		this.fourthStaircase = new SimpleListProperty<>(FXCollections.observableArrayList());

		// points set modification triggers RCH computation
		//
		this.points.addListener((Change<? extends Point> change) -> {
			while (change.next()) {
				if (change.wasAdded() || change.wasRemoved()) {
					update();
					return;
				}
			}
		});

		// changes in points coordinates trigger RCH computation
		//
		this.changeListener = (ObservableValue<? extends Number> observable,
				Number oldValue, Number newValue) -> {
					update();
				};
	}
	
	/**
	 * 
	 */
	public final void update() {
		computeRCH(this.pointsList, rch);
		
		this.overlaps.clear();
		this.overlaps.addAll(rch.overlaps);
		this.components.clear();
		this.components.addAll(rch.components);
		
		this.firstStaircase.setAll(rch.firstStaircase);
		this.secondStaircase.setAll(rch.secondStaircase);
		this.thirdStaircase.setAll(rch.thirdStaircase);
		this.fourthStaircase.setAll(rch.fourthStaircase);
	}
	
	/**
	 * @param points
	 * @param rch
	 */
	protected void computeRCH(LinkedList<Point> points, RCH rch) {
		RCH.computeRCH(points, rch);
	}

	/**
	 * @param point
	 */
	public final void addPoint(Point point) {
		
		if (!this.points.add(point)) return;
		
		// point coordinates modification triggers RCH computation
		//
		point.xProperty().addListener(this.changeListener);
		point.yProperty().addListener(this.changeListener);
	}

	/**
	 * @param point
	 */
	public final void removePoint(Point point) {
		
		if (this.points.remove(point)) return;
		
		// coordinates listener removal
		//
		point.xProperty().removeListener(this.changeListener);
		point.yProperty().removeListener(this.changeListener);
	}
	
	/**
	 * @return
	 */
	public final ReadOnlyListProperty<Double> firstStaircase() {
		return this.firstStaircase;
	}
	
	/**
	 * @return
	 */
	public final ReadOnlyListProperty<Double> secondStaircase() {
		return this.secondStaircase;
	}
	
	/**
	 * @return
	 */
	public final ReadOnlyListProperty<Double> thirdStaircase() {
		return this.thirdStaircase;
	}
	
	/**
	 * @return
	 */
	public final ReadOnlyListProperty<Double> fourthStaircase() {
		return this.fourthStaircase;
	}
	
	/**
	 * @return
	 */
	public final ReadOnlySetProperty<double []> components() {
		return this.components;
	}
	
	/**
	 * @return
	 */
	public final ReadOnlySetProperty<double []> overlaps() {
		return this.overlaps;
	}
}
