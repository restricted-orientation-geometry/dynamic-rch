/*
 * This file is part of Dynamic RCH.
 *
 * Copyright 2015 Carlos Alegría Galicia
 *
 * Dynamic RCH is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or any later version.
 *
 * Dynamic RCH is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Dynamic RCH. If not, see <http://www.gnu.org/licenses/>.
 */
package um.nibble.rch.model;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;


/**
 * A 2D point that can be marked as a maximal element
 * 
 * @author Carlos Alegría
 */
public class Point {
	
	private final BooleanProperty maximalProperty;
	private final DoubleProperty xProperty;
	private final DoubleProperty yProperty;
	
	/**
	 * @param x			The x-axis coordinate
	 * @param y			The y-axis coordinate
	 * @param maximal	If true, point is maximal, false otherwise
	 */
	public Point(double x, double y, boolean maximal) {
		this.maximalProperty = new SimpleBooleanProperty(maximal);
		this.xProperty = new SimpleDoubleProperty(x);
		this.yProperty = new SimpleDoubleProperty(y);
	}
	
	public Point() {
		this(0,0,false);
	}
	
	public Point(double x, double y) {
		this(x, y, false);
	}
	
	public Point(Point point) {
		this(point.getX(), point.getY(), point.isMaximal());
	}
	

	public final boolean isMaximal() {
		return this.maximalProperty.get();
	}
	
	public final void setMaximal(boolean maximal) {
		this.maximalProperty.set(maximal);
	}
	
	public BooleanProperty maximalProperty() {
		return this.maximalProperty;
	}
	
	
	public final double getX() {
		return this.xProperty.get();
	}
	
	public final void setX(double x) {
		this.xProperty.set(x); 
	}
	
	public DoubleProperty xProperty() {
		return this.xProperty;
	}
	
	
	public final double getY() {
		return this.yProperty.get();
	}
	
	public final void setY(double y) {
		this.yProperty.set(y);
	}
	
	public DoubleProperty yProperty() {
		return this.yProperty;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Point(" + xProperty.get() + ", " + yProperty.get() + ", "
				+ maximalProperty.get() + ")";
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 * 
	 * Method based on Point2D.equals
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (!(obj instanceof Point)) return false;
		
		Point that = (Point) obj;
		if (Double.doubleToLongBits(this.xProperty.get())
				!= Double.doubleToLongBits(that.xProperty.get())) {
			return false;
		}
		if (Double.doubleToLongBits(this.yProperty.get())
				!= Double.doubleToLongBits(that.yProperty.get())) {
			return false;
		}
		
		return true;
	}
}
