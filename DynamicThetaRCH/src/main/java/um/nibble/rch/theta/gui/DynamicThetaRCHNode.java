/*
 * This file is part of Dynamic Theta RCH.
 *
 * Copyright 2015 Carlos Alegría Galicia
 *
 * Dynamic Theta RCH is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or any later version.
 *
 * Dynamic Theta RCH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Dynamic Theta RCH. If not, see <http://www.gnu.org/licenses/>.
 */
package um.nibble.rch.theta.gui;

import javafx.beans.binding.IntegerBinding;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.text.Text;
import um.nibble.rch.gui.DynamicRCHNode;
import um.nibble.rch.model.DynamicRCHModel;
import um.nibble.rch.theta.model.DynamicThetaRCHModel;

/**
 * @author Carlos Alegría Galicia
 *
 */
public class DynamicThetaRCHNode extends DynamicRCHNode {
	
	private static final String ORIENTATION_LABEL_ID = "orientation_label";
	private static final KeyCombination ANGLE_TOGGLE = KeyCombination.valueOf("ctrl+t");
	
	private final Text angleLabel;
	private final DynamicThetaRCHModel model;
//	private final Logger logger = LoggerFactory.getLogger(DynamicRotatingRCHNode.class);
	
	
	/**
	 * 
	 */
	public DynamicThetaRCHNode() {
		super(new DynamicThetaRCHModel());
		
		//
		// setting up model
		//
		
		this.model = (DynamicThetaRCHModel) super.getModel();
		
		//
		// beta label
		//
		// binding to model angle to automate value update
		//
		
		this.angleLabel = new Text();
		this.angleLabel.setId(ORIENTATION_LABEL_ID);
		this.angleLabel.textProperty().bind(
				new SimpleStringProperty(UnicodeCharacters.THETA + " = ")
				.concat(IntegerBinding.integerExpression(this.model.angleProperty()))
				.concat(new SimpleStringProperty(UnicodeCharacters.DEGREES)));
		getChildren().add(angleLabel);
		
		
		//
		// scrolling changes orientation
		//
		
		this.setOnScroll((final ScrollEvent event) -> {
			this.model.stepAngle(event.getDeltaY() > 0);
		});
		

		//
		// key events configure visualization
		//
		
		setOnKeyPressed((final KeyEvent event) -> {
			
			// toggle angle and area label visibility
			//
			if (ANGLE_TOGGLE.match(event)) {
				this.angleLabel.setVisible(!this.angleLabel.isVisible());
				event.consume();
			}
		});
	}
	
	/**
	 * @return
	 */
	public DynamicRCHModel getModel() {
		return this.model;
	}
}
