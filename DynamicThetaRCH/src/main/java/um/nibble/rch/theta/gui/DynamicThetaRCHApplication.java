/*
 * This file is part of Dynamic Theta RCH.
 *
 * Copyright 2015 Carlos Alegría Galicia
 *
 * Dynamic Theta RCH is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or any later version.
 *
 * Dynamic Theta RCH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Dynamic Theta RCH. If not, see <http://www.gnu.org/licenses/>.
 */
package um.nibble.rch.theta.gui;

import um.nibble.rch.gui.DynamicRCHApplication;
import um.nibble.rch.gui.DynamicRCHNode;

/**
 * @author Carlos Alegría Galicia
 *
 */
public class DynamicThetaRCHApplication extends DynamicRCHApplication {

	@Override
	protected String getTitle() {
		return "Dynamic " + UnicodeCharacters.THETA + " RCH";
	}

	@Override
	protected DynamicRCHNode getNode() {
		return new DynamicThetaRCHNode();
	}

	@Override
	protected String[] getCSS() {
		return new String[] {"dynamic_theta_rch.css"};
	}
}
