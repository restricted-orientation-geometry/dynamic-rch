/*
 * This file is part of Dynamic Beta RCH.
 *
 * Copyright 2015 Carlos Alegría Galicia
 *
 * Dynamic Beta RCH is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or any later version.
 *
 * Dynamic Beta RCH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Dynamic Beta RCH. If not, see <http://www.gnu.org/licenses/>.
 */
package um.nibble.rch.beta;

import java.io.InputStream;
import java.util.logging.LogManager;

import javafx.application.Application;
import um.nibble.rch.beta.gui.DynamicBetaRCHApplication;

/**
 * @author Carlos Alegría Galicia
 *
 */
public class DynamicBetaRCH {

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {

		//
		// configuring logging system
		//

		try (InputStream in = ClassLoader.getSystemResourceAsStream("logging.properties")) {
			LogManager.getLogManager().readConfiguration(in);
		}

		//
		// executing application
		//

		Application.launch(DynamicBetaRCHApplication.class, args);
	}
}
