/*
 * This file is part of Dynamic Beta RCH.
 *
 * Copyright 2015 Carlos Alegría Galicia
 *
 * Dynamic Beta RCH is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or any later version.
 *
 * Dynamic Beta RCH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Dynamic Beta RCH. If not, see <http://www.gnu.org/licenses/>.
 */
package um.nibble.rch.beta.gui;

/**
 * @author Carlos Alegría Galicia
 *
 */
public final class UnicodeCharacters {

	public static final String BETA = "\u03B2";
	public static final String BETA_SUBSCRIPT = "\u1D66";
	public static final String DEGREES = "\u00B0";
	public static final String TWO_SUPERSCRIPT = "\u00B2";
	
	private UnicodeCharacters() {
	}
}
