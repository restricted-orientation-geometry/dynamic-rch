/*
 * This file is part of Dynamic Beta RCH.
 *
 * Copyright 2015 Carlos Alegría Galicia
 *
 * Dynamic Beta RCH is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or any later version.
 *
 * Dynamic Beta RCH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Dynamic Beta RCH. If not, see <http://www.gnu.org/licenses/>.
 */
package um.nibble.rch.beta.gui;

import javafx.scene.image.Image;
import javafx.stage.Stage;
import um.nibble.rch.gui.DynamicRCHApplication;
import um.nibble.rch.gui.DynamicRCHNode;

/**
 * @author Carlos Alegría Galicia
 *
 */
public class DynamicBetaRCHApplication extends DynamicRCHApplication {

	@Override
	protected void configureStage(Stage stage) {
		Image icons[] = new Image[] {
				new Image(ClassLoader.getSystemResourceAsStream("icon_16x16.png")),
				new Image(ClassLoader.getSystemResourceAsStream("icon_24x24.png")),
				new Image(ClassLoader.getSystemResourceAsStream("icon_32x32.png")),
				new Image(ClassLoader.getSystemResourceAsStream("icon_48x48.png")),
				new Image(ClassLoader.getSystemResourceAsStream("icon_128x128.png")),
				new Image(ClassLoader.getSystemResourceAsStream("icon_256x256.png"))};
		stage.getIcons().addAll(icons);
	}

	@Override
	protected String getTitle() {
		return "Dynamic O" + UnicodeCharacters.BETA_SUBSCRIPT + "-hull";
	}

	@Override
	protected DynamicRCHNode getNode() {
		return new DynamicBetaRCHNode();
	}

	@Override
	protected String[] getCSS() {
		return new String[] {"dynamic_beta_rch.css"};
	}
}
