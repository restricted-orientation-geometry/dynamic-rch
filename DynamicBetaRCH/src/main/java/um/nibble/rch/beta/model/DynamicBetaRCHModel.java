/*
 * This file is part of Dynamic Beta RCH.
 *
 * Copyright 2015 Carlos Alegría Galicia
 *
 * Dynamic Beta RCH is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or any later version.
 *
 * Dynamic Beta RCH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Dynamic Beta RCH. If not, see <http://www.gnu.org/licenses/>.
 */
package um.nibble.rch.beta.model;

import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.function.Consumer;

import javafx.beans.binding.DoubleBinding;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Point2D;
import javafx.scene.transform.Shear;
import um.nibble.rch.model.DynamicRCHModel;
import um.nibble.rch.model.Point;
import um.nibble.rch.model.RCH;

/**
 * @author Carlos Alegría Galicia
 *
 */
public class DynamicBetaRCHModel extends DynamicRCHModel {

	private static final double ANGLE_STEP = 1;
	private static final double ANGLE_INITIAL_VALUE = 90;
	private static final double ANGLE_MAX = 180;
	
	// the rotation angle, in degrees
	//
	private final DoubleProperty angle;
	
	// rotation transformations
	//
	private final Shear shear;
	private final Shear invShear;
	private final Consumer<double[]> compInvRotators;
	private final Consumer<List<Double>> strInvRotators;
	
	// rotated points list
	//
	private final LinkedList<Point> sPoints;
	
	// logging
	//
//	private final Logger logger = LoggerFactory.getLogger(DynamicRotatingRCHModel.class);
	
	
	/**
	 * 
	 */
	public DynamicBetaRCHModel() {
		super();
		
		// angle modification triggers staircase computation
		//
		this.angle = new SimpleDoubleProperty(ANGLE_INITIAL_VALUE);
		this.angle.addListener((ObservableValue<? extends Number> observable,
				Number oldValue, Number newValue) -> {
					update();
				});
		
		// shear operation is bound to angle value
		//
		this.shear= new Shear();
		this.shear.xProperty().bind(new DoubleBinding() {

			{
				super.bind(angle);
			}

			@Override
			protected double computeValue() {
				// transformation to project X coordinate along rotated Y-axis 
				//
				return -1 / StrictMath.tan(StrictMath.toRadians(angle.get()));
			}
		});
		
		
		// inverse shear is bound to the negative of shear x property
		//
		this.invShear = new Shear();
		this.invShear.xProperty().bind(this.shear.xProperty().negate());
		
		this.compInvRotators = (double[] comp) -> {
			for (int i = 0; i < comp.length; i += 2) {
				Point2D point = this.invShear.transform(comp[i], comp[i + 1]);
				comp[i] = point.getX();
				comp[i + 1] = point.getY();
			}
		};
		this.strInvRotators = (List<Double> str) -> {
			final ListIterator<Double> it = str.listIterator();
			
			while(it.hasNext()) {
				
				Point2D point = this.invShear.transform(it.next(),it.next());
				
				// roll back
				//
				it.previous();
				it.previous();
				
				// set values
				//
				it.next();
				it.set(point.getX());
				it.next();
				it.set(point.getY());
			}
		};
		
		this.sPoints = new LinkedList<>();
	}
	
	@Override
	protected void computeRCH(LinkedList<Point> points, RCH rch) {
		
		//
		// rotating points
		//
		
		this.sPoints.clear();
		points.forEach(p -> {
			Point2D point2D = this.shear.transform(p.getX(), p.getY());
			Point point = new Point(point2D.getX(), point2D.getY());
			p.maximalProperty().bind(point.maximalProperty());
			
			this.sPoints.add(point);
		});

		//
		// compute RCH
		//
		
		super.computeRCH(this.sPoints, rch);
		
		//
		// inverse-rotating resulting components / overlaps
		//

		rch.overlaps.forEach(ov -> this.compInvRotators.accept(ov));
		rch.components.forEach(comp -> this.compInvRotators.accept(comp));
		this.strInvRotators.accept(rch.firstStaircase);
		this.strInvRotators.accept(rch.secondStaircase);
		this.strInvRotators.accept(rch.thirdStaircase);
		this.strInvRotators.accept(rch.fourthStaircase);
	}
	
	/**
	 * @return
	 */
	public ReadOnlyDoubleProperty angleProperty() {
		return this.angle;
	}

	/**
	 * @return
	 */
	public double getAngle() {
		return this.angle.get();
	}
	
	/**
	 * @param angle
	 */
	public final void setAngle(double angle) {
		
		angle = ((angle % ANGLE_MAX) + ANGLE_MAX) % ANGLE_MAX;
		
		// avoid RCH computation by setting the same angle
		//
		if (angle == this.angle.get()) return;

		// limit angle to [0,2PI]
		//
		this.angle.set(angle);
	}
	
	/**
	 * @param direction
	 */
	public void stepAngle(boolean direction) {
		setAngle(this.angle.get() + (direction ? ANGLE_STEP : -ANGLE_STEP));
	}
}
